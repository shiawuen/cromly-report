
var debug = require('debug')('cromly-data:rooms')
var moment = require('moment')

var helper = require('./helper')
var db = require('./db')

var table = db.tables.rooms;
var slice = Array.prototype.slice.call.bind(Array.prototype.slice)

/**
 * Get summary of our portfolio-rooms
 *
 * @param {String|Number} accountId
 * @param {Function} next
 *
 **/

exports.summary = function(accountId, next) {
  // preprocess arguments
  if ('function' == typeof accountId) {
    next = accountId
    accountId = undefined
  }

  if (!next) throw new Error('expecting callback to be pass in')

  var p = db.tables.portfolios
  var joins = table
    .join( p )
    .on( p.id.equals( table.portfolio_id ) )

  var sqls = helper.generateCountsQuery( table, joins )

  if (accountId)
    Object.keys(sqls).forEach(function (key) {
      sqls[ key ] = sqls[ key ]
        .where( p.owner_id.equals( accountId ) )
    })

  helper.queryCounts( sqls, next )
}

exports.accountSummary = exports.summary

exports.all = helper.createQueryAll( table )