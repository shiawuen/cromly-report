
var debug = require('debug')('cromly-data:rooms')
var moment = require('moment')

var helper = require('./helper')
var db = require('./db')

var tables = db.tables;
var table = tables.catalogueParts;
var slice = Array.prototype.slice.call.bind(Array.prototype.slice)

/**
 * Get summary of our catalogue-parts
 *
 * @param {String|Number} accountId
 * @param {Function} next
 *
 **/

exports.summary = function(accountId, next) {
  // preprocess arguments
  if ('function' == typeof accountId) {
    next = accountId
    accountId = undefined
  }

  if (!next) throw new Error('expecting callback to be pass in')

  var c = tables.catalogues;
  var acc = tables.accounts;
  var joins = table
    .join( c )
    .on( c.id.equals( table.catalogue_id ) )
    .join( acc )
    .on( acc.id.equals( c.owner_id ) );

  var sqls = helper.generateCountsQuery( table, joins )

  if (accountId)
    Object.keys(sqls).forEach(function (key) {
      sqls[key] = sqls[key].where( acc.id.equals( accountId ) )
    })

  helper.queryCounts(sqls, next)
}

exports.accountSummary = exports.summary

exports.all = helper.createQueryAll( table )
