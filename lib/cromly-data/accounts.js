
var debug = require('debug')('cromly-data:accounts')
var moment = require('moment')

var db = require('./db')
var helper = require('./helper')

// query builder
var table = db.tables.accounts;

var handleRowsQuery = function (next) {
  return function (err, result) {
    if (err) {
      debug( 'error %s', err.stack )
      return next(err)
    }

    next( null, result.rows )
  }
}


exports.columns = table.columns
  .map(function (col) { return col.property })


exports.info = helper.createInfoQuery( table )


exports.summary = helper.createCountSummary( table )

/**
 * Query for all of the users
 **/

exports.all = function (next) {
  debug( 'query for all users' )

  var sql = helper.baseQuery( table )
  return helper.execQuery( sql, next )
}



/**
 * Query for all of the users as designer
 **/

exports.designers = function (next) {

  debug( 'query for all users who are designer' )

  var sql = helper.baseQuery( table )
    .where( table.designer.equals( true ) )
    .order( table.fullname.asc )

  return helper.execQuery( sql, next )
}


/**
 * Query for all of the users as merchant
 **/

exports.merchants = function (next) {

  debug( 'query for all users who are merchant' )

  var sql = helper.baseQuery( table )
    .where( table.merchant.equals( true ) )
    .order( table.date_created.desc )

  return helper.execQuery( sql, next )
}


/**
 * Query for all of the users as designer OR merchant
 **/

exports.designersAndMerchants = function (next) {

  debug( 'query for all users who are both merchant and designer' )

  var sql = helper.baseQuery( table )
    .where( table.merchant.equals( true ) )
    .or( table.designer.equals( true ) )
    .order( table.date_created.desc )

  return helper.execQuery( sql, next )
}

