
var debug = require('debug')('cromly-data:rooms')
var moment = require('moment')

var helper = require('./helper')
var db = require('./db')

var tables = db.tables;
var table = tables.roomParts;
var slice = Array.prototype.slice.call.bind(Array.prototype.slice)

/**
 * Get summary of our portfolio-parts
 *
 * @param {String|Number} accountId
 * @param {Function} next
 *
 **/

exports.summary = function(accountId, next) {
  // preprocess arguments
  if ('function' == typeof accountId) {
    next = accountId
    accountId = undefined
  }

  if (!next) throw new Error('expecting callback to be pass in')

  var pt = tables.roomParts;
  var r = tables.rooms;
  var p = tables.portfolios;
  var acc = tables.accounts;
  var joins = pt.join( r )
    .on( r.id.equals( pt.portfolio_room_id ) )
    .join( p )
    .on( p.id.equals( r.portfolio_id ) )
    .join( acc )
    .on( acc.id.equals( p.owner_id.cast('bigint') ) );

  var sqls = helper.generateCountsQuery(pt, joins)

  if (accountId)
    Object.keys(sqls).forEach(function (key) {
      sqls[key] = sqls[key].where( acc.id.equals( accountId ) )
    })

  helper.queryCounts(sqls, next)
}

exports.accountSummary = exports.summary

exports.all = helper.createQueryAll( table )
