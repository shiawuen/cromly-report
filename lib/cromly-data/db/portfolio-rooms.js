
var sql = require('sql')

// table definition with columns that we interested in
var table = sql.define({

  // table name
  name: 'portfolio_room',

  // columns we interested in
  columns: [
    'id',
    'title',

    // groupings of the portfolio_room
    'style_name',
    'room_type',

    // reference key
    'portfolio_id',

    // timestamps
    'date_created',
    'last_updated',

    // flags
    'published',
    'featured',

    // tagging
    'tags'
  ]

})

// expose out the account table
module.exports = table

