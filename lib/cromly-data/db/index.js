
// consolidate the API into one
module.exports = {
  // the client for use to talk to the DB
  client: require('./client'),

  // tables for us to do query qith
  tables: {

    accounts: require('./accounts'),

    portfolios: require('./portfolios'),

    rooms: require('./portfolio-rooms'),

    roomParts: require('./portfolio-parts'),

    catalogues: require('./catalogues'),

    catalogueParts: require('./catalogue-parts'),

  }

}