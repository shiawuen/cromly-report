
var debug = require('debug')('cromly-data:db')
var pg = require('pg')

// queue for us to buffer queries and wait for
// DB to be connect
var queue = [];

// flag to check if the client is connected to DB
var connected = false;

debug( 'establishing db connection' )

var env = process.env.NODE_ENV || 'development';
var config;

if ('production' == env)
  config = {
    user: 'cromly',
    password: 'cromly',
    host: '172.31.38.36',
    database: 'cromly',
    port: '5432'
  }
else
  config = {
    user: 'cromly',
    password: '',
    host: 'localhost',
    database: 'cromly',
    port: '5432'
  };


// setup for our DB connection
var client = new pg.Client(config)

client.connect(function (err) {
  if (err) {
    throw err
    return
  }

  debug( 'connected' )
})

// expose out the client connection
module.exports = client
