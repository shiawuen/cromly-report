
var sql = require('sql')

// table definition with columns that we interested in
var table = sql.define({

  // table name
  name: 'account',

  // columns we interested in
  columns: [
    'id',
    'fullname',
    'email',
    'image_url',
    'designer',
    'merchant',
    'date_created',
  ]

})

// expose out the account table
module.exports = table

