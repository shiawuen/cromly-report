
var sql = require('sql')

// table definition with columns that we interested in
var table = sql.define({

  // table name
  name: 'catalogue_part',

  // columns we interested in
  columns: [
    'id',

    // remote location
    'cdn_normal_image_url',

    // reference key
    'catalogue_id',

    // timestamps
    'date_created',
    'last_updated',

    // dimension
    'width',
    'height',
  ]

})

// expose out the account table
module.exports = table
