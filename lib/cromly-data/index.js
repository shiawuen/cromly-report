
// consolidate our DB queries helpers into one
module.exports = {

  accounts: require('./accounts'),

  portfolios: require('./portfolios'),

  rooms: require('./rooms'),

  roomParts: require('./room-parts'),

  catalogues: require('./catalogues'),

  catalogueParts: require('./catalogue-parts'),

}
