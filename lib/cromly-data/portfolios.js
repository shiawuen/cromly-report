
var moment = require('moment')

var helper = require('./helper')
var db = require('./db')

var table = db.tables.portfolios;

/**
 * Get summary of our portfolios
 *
 * @param {Function} next
 *
 **/

exports.summary = helper.createCountSummary(table);
exports.accountSummary = helper.createCountSummary(table, 'owner_id');

exports.all = helper.createQueryAll( table )
exports.accountAll = helper.createQueryAll( table, 'owner_id' )