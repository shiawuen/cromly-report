
var koa = require('koa')
var cors = require('koa-cors')

var app = module.exports = koa()

app.use(cors({
  origin: [
    'http://192.168.22.35:3001',
    'http://localhost:3001',
  ]
}))

app.use(function *notfound (next) {
  yield* next

  if (404 != this.status) return

  this.status = 404;

  if ('string' == typeof this.body)
    this.body = {
      status: 404,
      error: "Not Found",
      message: this.body
    }
})

require('./routes')
.forEach(function (route) {
  app.use( route )
})
