
var _ = require('koa-route')
var portfolios = require('cromly-data').portfolios

exports.summary = _.get('/portfolios/summary', function* () {
  this.body = yield summary()
})

// thunkify portfolio summary
function summary () {
  return function (next) {
    portfolios.summary( next )
  }
}

