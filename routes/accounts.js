
var _ = require('koa-route')
var Joi = require('joi')

var db = require('cromly-data');
var accounts = db.accounts;

var summaries = {
  portfolios: {
    project: thunk( db.portfolios ),
    rooms: thunk( db.rooms ),
    images: thunk( db.roomParts )
  },
  catalogues: {
    product: thunk( db.catalogues ),
    images: thunk( db.catalogueParts )
  }
}

exports.info = _.get('/account/:id', function* (id) {
  var spec = Joi.number()
  var user = yield userinfo(id)

  if ( ! user ) {
    this.status = 404
    this.body = 'account not found'
    return
  }

  var p = summaries.portfolios;
  var c = summaries.catalogues;

  // convert date to timestamp
  user.date_created = +user.date_created;

  user.summary = {
    portfolios: {
      project: getTotal( yield p.project( id ) ),
      rooms: getTotal( yield p.rooms( id ) ),
      images: getTotal( yield p.images( id ) )
    },
    catalogues: {
      product: getTotal( yield c.product( id ) ),
      images: getTotal( yield c.images( id ) )
    }
  }

  this.body = user

})

exports.summary = _.get('/accounts/summary', function* () {
  this.body = yield stats()
})

exports.list = _.get('/accounts/:role', function* (role) {
  var spec = Joi.any().valid('merchants').valid('designers')
  this.body = yield usersByrole(role)
})


function thunk (obj) {
  return function (id) {
    return function (next) {
      obj.accountSummary( id, next )
    }
  }
}

function getTotal (summary) {
  return summary.total
}

function userinfo (id) {
  return function (next) {
    accounts.info( id, next )
  }
}

function usersByrole (role) {
  return function (next) {
    accounts[ role ]( next )
  }
}

function stats () {
  return function (next) {
    accounts.summary( next )
  }
}
