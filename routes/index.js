
// routes objects

var accounts = require('./accounts')
var portfolios = require('./portfolios')
var catalogues = require('./catalogues')

// consolidated routes

var routes = [
  accounts,
  portfolios,
  catalogues
]
.map(function (mod) {
  return Object.keys( mod ).map(function (key) {
    return mod[ key ]
  })
})
.reduce(function (routes, group) {
  return routes.concat(group)
}, [])


// exposing roustes

module.exports = routes
