
var _ = require('koa-route')
var catalogues = require('cromly-data').catalogues

exports.summary = _.get('/catalogues/summary', function* () {
  this.body = yield summary()
})

// thunkify portfolio summary
function summary () {
  return function (next) {
    catalogues.summary( next )
  }
}

